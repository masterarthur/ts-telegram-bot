import { connect } from 'mongoose'
import * as dotenv from 'dotenv'

import bot from './lib/bots/bot'
import UserLocale from './lib/models/UserLocale'

const { NODE_ENV } = process.env

if (NODE_ENV !== 'production') {
  dotenv.config()
}

const { MONGO } = process.env;

(async () => {
  try {
    await connect(MONGO, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      autoIndex: NODE_ENV !== 'production',
    })
    await UserLocale.initLocales()
    await bot.launch()
  } catch (err) {
    console.error(err)
    process.exit(1)
  }
  console.log(`Bot is started at ${new Date().toLocaleString()}`)
})()
