FROM node:14.16.0-alpine3.10

WORKDIR /usr/app/src

COPY package*.json ./

RUN npm install

COPY . .

ENTRYPOINT npm run start
