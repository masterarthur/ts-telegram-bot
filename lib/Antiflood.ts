export default interface Antiflood {
  [key: number]: {
    [key: number]: number
  }
}
