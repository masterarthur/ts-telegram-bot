import {
  Schema, model, Document, Model,
} from 'mongoose'

export interface TextDocument extends Document {
  locale: string
  key: string
  value: string
}

export interface TextModel extends Model<TextDocument, TextDocument> {}

const TextSchema: Schema<TextDocument, TextModel> = new Schema<
TextDocument,
TextModel
>({
  locale: {
    type: String,
    required: true,
  },
  key: {
    type: String,
    required: true,
  },
  value: {
    type: String,
    required: true,
  },
})

const Text: TextModel = model<TextDocument, TextModel>('Text', TextSchema)

export default Text
