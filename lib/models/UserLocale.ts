import Text from './Text'

export type LocaleDefined = true
export type Locale = string

interface Locales {
  [key: string]: LocaleDefined
}

class UserLocale {
  static defaultLocale = 'en'

  static locales: Locales = {}

  static getLocale(locale: Locale) {
    if (this.locales[locale]) {
      return locale
    }

    return this.defaultLocale
  }

  static async initLocales() {
    this.locales[this.defaultLocale] = true

    const allLocales = await Text.distinct('locale').exec()

    allLocales.forEach((locale) => {
      this.locales[locale] = true
    })
  }
}

export default UserLocale
