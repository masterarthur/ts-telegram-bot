import {
  Schema, model, Document, Model,
} from 'mongoose'

export interface UserDocument extends Document {
  id: number
  is_bot: boolean
  firstName: string
  lastName?: string
  fullName: string
  link: string
  username?: string
  language?: string
  blocked: boolean
}

export interface UserModel extends Model<UserDocument> {
  findByTelegramId(userId: number): Promise<UserDocument>
}

const UserSchema: Schema<UserDocument, UserModel> = new Schema<
  UserDocument,
  UserModel
>({
  id: {
    type: Number,
    required: true,
    unique: true,
  },
  firstName: {
    type: String,
    required: true,
    alias: 'first_name',
  },
  lastName: {
    type: String,
    alias: 'last_name',
    default: '',
  },
  username: {
    type: String,
    default: '',
  },
  blocked: {
    type: Boolean,
    default: false,
  },
  language: {
    type: String,
    alias: 'language_code',
  },
})

UserSchema.virtual('fullName').get(function () {
  return `${this.firstName}${this.lastName != '' ? ` ${this.lastName}` : ''}`
})

UserSchema.virtual('link').get(function () {
  return this.username != ''
    ? `https://t.me/${this.username}`
    : `tg://user?id=${this.id}`
})

UserSchema.statics.findByTelegramId = async function (userId: number) {
  return this.findOne({ id: userId })
}

const User: UserModel = model<UserDocument, UserModel>('User', UserSchema)

export default User
