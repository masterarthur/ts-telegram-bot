import { Composer } from 'telegraf'

import BotContext from '../../bots/BotContext'
import Text from '../../models/Text'
import UserLocale from '../../models/UserLocale'

const textsComposer = new Composer<BotContext>()

textsComposer.command('settext', async (ctx) => {
  const { args, isBotAdmin } = ctx

  if (!isBotAdmin) {
    return
  }

  const match = args.match(/^(?<textKey>[a-z0-9_-]+)\s(?<locale>[a-z_]{2,}) /i)

  const textKey = match?.groups.textKey ?? ''
  const locale = match?.groups.locale ?? ''
  const textToAdd = args.substring(textKey.length + locale.length + 2)

  if (locale && textKey && textToAdd) {
    const text = new Text({
      locale,
      key: textKey,
      value: textToAdd,
    })

    try {
      await text.save()
      await UserLocale.initLocales()

      ctx.reply('OK')
    } catch (e) {
      console.error(e)
      ctx.reply('Something went wrong')
    }
  } else {
    ctx.reply('Invalid command syntax')
  }
})

textsComposer.command('gettext', async (ctx) => {
  const { args, isBotAdmin } = ctx

  if (!isBotAdmin) {
    return
  }

  const match = args.match(/^(?<textKey>[a-z0-9_-]+)\s(?<locale>[a-z_]{2,})/i)

  const textKey = match?.groups.textKey ?? ''
  const locale = match?.groups.locale ?? ''

  if (locale && textKey) {
    try {
      const text = await Text.findOne({
        locale,
        key: textKey,
      })

      if (text) {
        ctx.reply(`/settext ${textKey} ${locale} ${text.value}`)
      } else {
        ctx.reply('Text was not found')
      }
    } catch (e) {
      console.error(e)
      ctx.reply('Something went wrong')
    }
  } else {
    ctx.reply('Invalid command syntax')
  }
})

export default textsComposer
