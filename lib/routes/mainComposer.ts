import { Composer } from 'telegraf'

import BotContext from '../bots/BotContext'
import './texts'

const mainComposer = new Composer<BotContext>()

mainComposer.command('start', async (ctx) => {
  await ctx.reply(await ctx.text('start_text'))
})

export default mainComposer
