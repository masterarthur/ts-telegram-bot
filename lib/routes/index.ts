import localesComposer from './locales'
import mainComposer from './mainComposer'
import textsComposer from './texts'

mainComposer.use(textsComposer)
mainComposer.use(localesComposer)

export default mainComposer
