import { Composer } from 'telegraf'

import BotContext from '../../bots/BotContext'
import UserLocale from '../../models/UserLocale'

const localesComposer = new Composer<BotContext>()

localesComposer.command('getlocales', async (ctx) => {
  if (!ctx.isBotAdmin) return

  const localesNames = Object.keys(UserLocale.locales)
  const localesText = localesNames.map((localeName) => `<code>${localeName}</code>`).join(', ')

  await ctx.reply(
    `Locales: ${localesText}`,
    {
      parse_mode: 'HTML'
    },
  )
})

export default localesComposer
