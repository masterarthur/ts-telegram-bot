import BotContext from '../bots/BotContext'

const botContextUserId = async (ctx: BotContext, next: () => Promise<void>) => {
  const {
    from: { id: userId },
  } = ctx

  ctx.userId = userId

  await next()
}

export default botContextUserId
