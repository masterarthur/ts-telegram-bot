import BotContext from '../bots/BotContext'
import { NOT_FOUND_INDEX } from '../constants'

const botContextCommands = async (ctx: BotContext, next: () => Promise<void>) => {
  const {
    message: { text, entities },
    userId,
  } = ctx

  ctx.command = ''
  ctx.args = ''

  if (entities.length > 0 && text) {
    const commandEntity = entities.find(
      (entity) => entity.offset === 0 && entity.type === 'bot_command',
    )

    if (commandEntity) {
      ctx.command = text.substring(1, commandEntity.length).trim()

      // clearing username from command
      const emailDogIndex = ctx.command.indexOf('@')
      if (emailDogIndex !== NOT_FOUND_INDEX) {
        ctx.command = ctx.command.substring(0, emailDogIndex)
      }

      ctx.args = text.substring(commandEntity.length).trim()

    }
  }

  await next()
}

export default botContextCommands
