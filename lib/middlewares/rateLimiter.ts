import Antiflood from '../Antiflood'
import BotContext from '../bots/BotContext'

const antiflood: Antiflood = {}

const rateLimiter = async (ctx: BotContext, next: () => Promise<void>) => {
  const {
    userId
  } = ctx

  const now = Math.floor(Date.now() / 1000)

  if (antiflood[userId] === undefined) {
    antiflood[userId] = {}
  }

  if (antiflood[userId][now] === undefined) {
    antiflood[userId][now] = 1
  } else {
    antiflood[userId][now] += 1
  }

  if (antiflood[userId][now] > 2) {
    return
  }

  await next()
}

export default rateLimiter
