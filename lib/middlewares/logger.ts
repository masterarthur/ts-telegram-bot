import BotContext from '../bots/BotContext';

const logger = async (ctx: BotContext, next: () => Promise<void>) => {
  const {
    updateType,
    userId,
    args,
    command,
  } = ctx

  const now = new Date()

  const logStart = `[${userId}][${updateType}][${now.toLocaleString()}]`

  if (args || command) {
    console.log(`${logStart} /${ctx.command} ${ctx.args}`)
  }
  try {
    await next()
  } catch (err) {
    console.error(`${logStart}[error]`, err)
  }
}

export default logger
