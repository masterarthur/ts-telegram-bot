import * as dotenv from 'dotenv'
import BotContext from '../bots/BotContext'

const { NODE_ENV } = process.env

if (NODE_ENV !== 'production') {
  dotenv.config()
}

const { ADMIN_ID } = process.env

const botContextIsBotAdmin = async (ctx: BotContext, next: () => Promise<void>) => {
  if (!ctx.isBotAdmin) {
    ctx.isBotAdmin = ctx.userId.toString() === ADMIN_ID
  }

  await next()
}

export default botContextIsBotAdmin
