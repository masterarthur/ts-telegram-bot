import BotContext from '../bots/BotContext'
import User from '../models/User'

const userLoader = async (ctx: BotContext, next: () => Promise<void>) => {
  const { userId, from } = ctx

  ctx.user = await User.findByTelegramId(userId)

  if (!ctx.user) {
    ctx.user = new User(from)
    await ctx.user.save()
  }

  await next()
}

export default userLoader
