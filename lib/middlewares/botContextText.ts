import BotContext from '../bots/BotContext'
import Text from '../models/Text'
import UserLocale from '../models/UserLocale'

const botContextText = async (ctx: BotContext, next: () => Promise<void>) => {
  const { user } = ctx

  if (!ctx.text) {
    ctx.text = async (key: string): Promise<string> => {
      const text = await Text.findOne({
        key,
        locale: UserLocale.getLocale(user.language),
      })

      if (text) {
        return text.value
      }

      return `${key}`
    }
  }

  await next()
}

export default botContextText
