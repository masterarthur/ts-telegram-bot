import { Context } from 'telegraf'
import { UserDocument } from '../models/User'

export default interface BotContext extends Context {
  command: string
  user: UserDocument
  userId: number
  args: string
  text(key: string): Promise<string>
  isBotAdmin: boolean
}
