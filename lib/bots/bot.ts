import { Telegraf } from 'telegraf'
import * as dotenv from 'dotenv'

import BotContext from './BotContext'
import rateLimiter from '../middlewares/rateLimiter'
import botQueue from '../queues/botQueue'
import botContextUserId from '../middlewares/botContextUserId'

const { NODE_ENV } = process.env

if (NODE_ENV !== 'production') {
  dotenv.config()
}

const { BOT_TOKEN } = process.env

const bot = new Telegraf<BotContext>(BOT_TOKEN)

bot.use(botContextUserId)
bot.use(rateLimiter)

bot.use((ctx) => {
  botQueue.add('update', ctx.update)
})

export default bot
