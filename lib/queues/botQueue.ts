import * as Bull from 'bull'
import * as dotenv from 'dotenv'
import { Telegraf } from 'telegraf'
import { Update } from 'telegraf/typings/telegram-types'

import router from '../routes'
import userLoader from '../middlewares/userLoader'
import botContextText from '../middlewares/botContextText'
import botContextIsBotAdmin from '../middlewares/botContextIsBotAdmin'
import botContextCommands from '../middlewares/botContextCommands'
import BotContext from '../bots/BotContext'
import botContextUserId from '../middlewares/botContextUserId'
import logger from '../middlewares/logger'

const { NODE_ENV } = process.env

if (NODE_ENV !== 'production') {
  dotenv.config()
}

const { BOT_TOKEN, BOT_USERNAME } = process.env

export const queuedBot = new Telegraf<BotContext>(BOT_TOKEN)

const botQueue = new Bull<Update>(`${BOT_USERNAME}-bot-queue`, {
  limiter: {
    max: 30,
    duration: 1000,
  },
})

botQueue.process('update', (job) => {
  const { data: update } = job

  queuedBot.handleUpdate(update)
})

queuedBot.use(botContextUserId)
queuedBot.use(userLoader)
queuedBot.use(botContextText)
queuedBot.use(botContextIsBotAdmin)
queuedBot.use(botContextCommands)
queuedBot.use(logger)

queuedBot.use(router)

export default botQueue
