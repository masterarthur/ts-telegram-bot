echo "Installing dependencies..."
npm i
touch ".env"
touch ".env.prod"
 
echo "Enter bot username:"
read BOT_USERNAME
echo "Enter production bot token: "
read PROD_BOT_TOKEN
echo "Enter development bot token: "
read DEV_BOT_TOKEN
echo "Enter your telegram id:"
read ADMIN_ID
echo "Enter logs chat id (you can use your tg id):"
read LOG_CHAT_ID

echo "BOT_TOKEN=$DEV_BOT_TOKEN
ADMIN_ID=$ADMIN_ID
MONGO=mongodb://127.0.0.1:27019/$BOT_USERNAME
REDIS=redis://127.0.0.1:6379
LOG_BOT_TOKEN=$DEV_BOT_TOKEN
LOG_CHAT_ID=$LOG_CHAT_ID" > ".env"


echo "BOT_TOKEN=$PROD_BOT_TOKEN
ADMIN_ID=$ADMIN_ID
MONGO=mongodb://mongo:27017/$BOT_USERNAME
REDIS=redis://redis:6379
LOG_BOT_TOKEN=$DEV_BOT_TOKEN
LOG_CHAT_ID=$LOG_CHAT_ID" > ".env.prod"

echo "version: \"3\"

services:
  mongo:
    image: mongo:4.2.6
    restart: always
    volumes:
      - \"${BOT_USERNAME}Data:/data/db\"
  redis:
    image: redis:6.0.3
  bot:
    build: .
    env_file:
      - .env.prod
    depends_on:
      - redis
      - mongo

volumes:
  ${BOT_USERNAME}Data:
" > "docker-compose.yml"

